﻿using System;
using System.Net;
using Newtonsoft.Json;
using RestSharp;
using System.Windows.Forms;

namespace CostOfBTC
{
	public partial class Form1 : Form
	{
		RestClient ClientAPI;		// Клиент для работы с API
		RestRequest Request;		// Запрос для API

		public Form1()
		{
			InitializeComponent();
		}

		// Настройка клиента и запроса
		private void Form1_Load(object sender, EventArgs e)
		{
			ClientAPI = new RestClient("https://api.binance.com");

			Request = new RestRequest("api/v3/ticker/price", Method.GET);

			// Интересующая пара
			Request.AddParameter("symbol", "BTCUSDT");

			// Установка рабочих протоколов
			ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
		}

		private void button1_Click(object sender, EventArgs e)
		{
			// Выполнение запроса
			IRestResponse response = ClientAPI.Execute(Request);

			// Выполнен ли запрос?
			if (response.ErrorException == null)
			{
				// Получение результата запроса
				String content = response.Content;

				// Конвертирование из JSON
				Сurrency res = JsonConvert.DeserializeObject<Сurrency>(content);

				// Вывод результата
				Cost.Text = res.price.ToString();
			}
			else
			{
				Cost.Text = response.ErrorMessage;
			}
		}
	}

	// Класс для перевода из JSON в понятную форму
	public class Сurrency
	{
		public string symbol;
		public float price;
	}
}
